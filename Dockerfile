FROM centos:latest
MAINTAINER Dinesh
RUN yum repolist
RUN yum install httpd -y
CMD ['/usr/sbin/httpd', '-D', 'FOREGROUND']
EXPOSE 80
